# Sentry in Kubernetes for MUNI Helps
  This is a forked repository of the[sentry-kubernetes repository](https://github.com/sentry-kubernetes/charts). It allows us to run Sentry in the Kubernetes cluster for the MUNI Helps app. This alone with custom values.yaml file (`custom_sentry_values.yaml`) should be enough to run it. However, there might be updates to the original repository in the future, and you will need to recreate this fork. So, this README file is a manual for how to recreate the fork and how to run it.

## Preparations
Before installation, you need to install and set up Kubectl and Helm.

For Kubectl, check [this](https://docs.cerit.io/docs/kubectl.html).


For Helm, check [this](https://helm.sh/docs/intro/install/), or if you have Ubuntu and Snap, run this command:
```
sudo snap install helm --classic
```

Also, you will need kubeseal to seal secrets. The Cerit manual is [here](https://docs.cerit.io/docs/kubectl.html).
But I found the manual in kubeseal [github repo](https://github.com/bitnami-labs/sealed-secrets) more helpful.
But if you have Linux all you need to do is go to [releases page](https://github.com/bitnami-labs/sealed-secrets/releases) and download the tar.xz file suitable for you (probably "kubeseal-VERSION_NUMBER-linux-amd64.tar.gz").
Then in folder where you downloaded it, run:
```
tar -xvzf kubeseal-VERSION_NUMBER-linux-amd64.tar.gz kubeseal
sudo install -m 755 kubeseal /usr/local/bin/kubeseal
```

## Create password as secret
Use `./secrets.yaml` file in this forked repo. Change `metadata.namespace` to your namespace name. Then run
```
echo <YOUR PASSWORD> | base64
```
This will print encoded password. Copy this value to `data.adminpassword`. Then run in this folder:
```
kubeseal --controller-namespace sealed-secrets-operator < secrets.yaml > sealed-secret.json
kubectl apply -f sealed-secret.json -n <YOUR NAMESPACE>
```
And that is it. Now you have your password stored as secret in your namespace. Together with `user.email` you will use it to log into Sentry.

## Forking the chart and creating custom values.yaml file
In the original repository focus only on `/charts/sentry/` subfolder, which deploys sentry inside Kubernetes. Other subfolders are about monitoring Kubernetes with Sentry.

It is all about configuring values. Values.yaml is a file by which you can alter the settings of the applications. This helm chart includes other smaller helm charts, and unfortunately, you cannot alter the values of all helm charts from the one values file.

### Creating values.yaml

Running this helm chart on the Cerit Kubernetes cluster needs to follow some rules. You need to limit CPU and RAM usage of containers to fit available resources. For example, it looks like this:
```
resources:
  limits:
    cpu: 200m
    memory: 700Mi
  requests:
    cpu: 150m
    memory: 400Mi
```

Also, most of the containers must have security context set this way:

```
securityContext:
  runAsUser: 1002
  allowPrivilegeEscalation: false
  capabilities:
    drop:
      - ALL
  runAsNonRoot: true
  seccompProfile:
    type: RuntimeDefault
```

Most of this is done in the `custom_sentry_values.yaml`. Besides resources and securityContext, it also alters some other settings. `rabbitmq.enabled` is set to false because we use Redis. Most importantly, ingress is set up for the Cerit cluster. In `ingress.tls.hosts` and `ingress.hostname`, you can set the name of the URL.

### Forking the chart

However, these global values cannot control some of the prerequisites in this chart. They are locked in .tgz files. That is why the fork is needed. In this fork is an `original-but-unzipped` branch that unzips all packages. On top of this branch is another branch, `forked-repository`, which alters values.yaml files in the unzipped packages. If you check changes in this branch, you find out which prerequisites must be altered. But it is also written here.

Download the original chart and unzip packages in `/charts/sentry/charts`. Each of the packages contains a values file. Modify these values:

Clickhouse values:

-   `clickhouse.securityContext` - use the snippet above.
    
-   `clickhouse.resources` - modify it with respect to available resources. You can try the snippet above.
    

Kafka values

-   `provisioning.containerSecurityContext` - set enabled to true and add variables from the code snippet above which are not already there.
    
-   `provisioning.resources` - modify it with respect to available resources. You can try the snippet above.
    

Nginx values:

-   `containerSecurityContext` - set enabled to true and add variables from the code snippet above which are not already there.
    
-   `resources` - modify it with respect to available resources. You can try the snippet above.
    

Postgresql values:

-   `primary.containerSecurityContext` - set enabled to true and add variables from the code snippet above which are not already there.
    
-   `primary.resources` - modify it with respect to available resources. You can try the snippet above.
    

Redis values:

-   `master.containerSecurityContext` - set enabled to true and add variables from the code snippet above which are not already there.
    
-   `master.resources` - modify it with respect to available resources. You can try the snippet above.
    
-   `replica.containerSecurityContext` - set enabled to true and add variables from the code snippet above which are not already there.
    
-   `replica.resources` - modify it with respect to available resources. You can try the snippet above.
    

Note: If you unzip the charts, it won't recognize them because the package has a subfolder. So, for example, when you unzip `clickhouse-3.5.0.tgz`, it will create a folder with the structure `clickhouse-3.5.0/clickhouse/charts`. This is wrong. The correct structure is `clickhouse-3.5.0/charts`.

## Running the chart
Now you should have the forked repository, custom values.yaml file, and set up Kubectl and Helm. With this, you can install the helm chart. For installation, run this command from the folder where you have the forked package:

```
helm install name-of-application ./sentry -n name-space -f ./custom_sentry_values.yaml --timeout 40m
```

Installation should take 25-30 minutes. That should be it. However, it is possible that some adjustments will be needed with future updates to the chart. In case of errors, read the messages and alter the global values file or the prerequisites. It is beneficial to check templates inside sentry/templates. Especially those with deployment in a name. It will tell you from which place the deployments take each value in the values file.

Note 1: sometimes, some jobs or deployments fail for no obvious reason. In this case, it helped to uninstall the chart, delete PVC and then install it again:

```
helm uninstall name-of-application -n name-space
kubectl delete pvc --all -n name-space
helm install name-of-application ./sentry -n name-space -f path/to/values.yaml --timeout 30m
```

Note 2: This command is very handy when setting up the memory and CPU limits and requests. It displays how much is limited and requested in each container.

```
kubectl get po -o custom-columns="Name:metadata.name,CPU-limit:spec.containers[*].resources.limits.cpu, CPU-request:spec.containers[*].resources.requests.cpu, memory-limits:spec.containers[*].resources.limits.memory, memory-request:spec.containers[*].resources.requests.memory" --namespace=name-space
```